import XCTest

import BGGKitTests

var tests = [XCTestCaseEntry]()
tests += BGGKitTests.__allTests()

XCTMain(tests)
