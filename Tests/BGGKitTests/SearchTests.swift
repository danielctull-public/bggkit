
import BGGKit
import Resourceful
import XCTest

final class SearchRequestTests: XCTestCase {

    func testBasic() {
        let search = Resource.search(query: "Search")
        XCTAssertEqual(
            search.request.url,
            URL(string: "https://www.boardgamegeek.com/xmlapi2/search?query=Search")
        )
    }

    func testType() {
        let search = Resource.search(query: "Search", types: [.boardGame])
        XCTAssertEqual(
            search.request.url,
            URL(string: "https://www.boardgamegeek.com/xmlapi2/search?query=Search&type=boardgame")
        )
    }

    func testTypes() {
        let search = Resource.search(query: "Search", types: [.boardGame, .boardGameAccessory])
        XCTAssertEqual(
            search.request.url,
            URL(string: "https://www.boardgamegeek.com/xmlapi2/search?query=Search&type=boardgame,boardgameaccessory")
        )
    }

    func testExact() {
        let search = Resource.search(query: "Search", exact: 3)
        XCTAssertEqual(
            search.request.url,
            URL(string: "https://www.boardgamegeek.com/xmlapi2/search?query=Search&exact=3")
        )
    }
}

final class SearchTransformTests: XCTestCase {

    func testSuccess() throws {
        let search = Resource.search(query: "Search")
        let data = result.data(using: .utf8)!
        let items = try search.transform((data, response))
        XCTAssertEqual(items.count, 5)
        XCTAssertEqual(items[0].name, "Patchwork")
        XCTAssertEqual(items[1].name, "Patchwork")
        XCTAssertEqual(items[2].name, "Patchwork di principesse")
        XCTAssertEqual(items[3].name, "Patchwork Doodle")
        XCTAssertEqual(items[4].name, "Patchwork Express")
    }
}

private let response = URLResponse(url: URL(string: "http://example.com")!,
                                   mimeType: nil,
                                   expectedContentLength: 0,
                                   textEncodingName: nil)

private let result = """
<?xml version="1.0" encoding="utf-8"?>
<items total="5" termsofuse="https://boardgamegeek.com/xmlapi/termsofuse">
    <item type="boardgame" id="163412">
        <name type="primary" value="Patchwork"/>
        <yearpublished value="2014" />
    </item>
    <item type="boardgame" id="41585">
        <name type="alternate" value="Patchwork"/>
        <yearpublished value="2009" />
    </item>
    <item type="boardgame" id="242544">
        <name type="alternate" value="Patchwork di principesse"/>
        <yearpublished value="2018" />
    </item>
    <item type="boardgame" id="264239">
        <name type="primary" value="Patchwork Doodle"/>
        <yearpublished value="2019" />
    </item>
    <item type="boardgame" id="246639">
        <name type="primary" value="Patchwork Express"/>
        <yearpublished value="2018" />
    </item>
</items>
"""
