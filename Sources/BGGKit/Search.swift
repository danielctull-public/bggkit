
import Foundation
import Resourceful
import XMLCoder

enum SearchXML {

    struct Result: Codable {

        let item: [Item]

        struct Item: Codable {

            let type: String
            let id: String
            let name: Name
            let yearpublished: YearPublished?

            struct Name: Codable {
                let value: String
                let type: String
            }

            struct YearPublished: Codable {
                let value: String
            }
        }
    }
}

extension Item {

    init(_ item: SearchXML.Result.Item) {
        identifier = item.id
        name = item.name.value
    }
}

public enum SearchType: String, Codable {
    case rpgItem = "rpgitem"
    case videoGame = "videogame"
    case boardGame = "boardgame"
    case boardGameAccessory = "boardgameaccessory"
    case boardGameExpansion = "boardgameexpansion"
}

extension Resource where Value == Data {

    public static func search(
        query: String,
        types: [SearchType]? = nil,
        exact: Int? = nil
    ) -> Resource<[Item]> {

        let base = URL.base.appendingPathComponent("search")
        var components = URLComponents(url: base, resolvingAgainstBaseURL: false)!

        var items = [URLQueryItem(name: "query", value: query)]

        if let types = types {
            let type = types.map { $0.rawValue }.joined(separator: ",")
            items.append(URLQueryItem(name: "type", value: type))
        }

        if let exact = exact {
            items.append(URLQueryItem(name: "exact", value: String(exact)))
        }

        components.queryItems = items
        let url = components.url!
        let request = URLRequest(url: url)
        return Resource(request: request) { $0.data }
            .tryMap { try XMLDecoder().decode(SearchXML.Result.self, from: $0) }
            .tryMap { $0.item.map(Item.init) }
    }
}
