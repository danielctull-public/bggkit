
import Foundation

extension URL {
    static let base = URL(string: "https://www.boardgamegeek.com/xmlapi2")!
}
