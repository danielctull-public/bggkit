// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "BGGKit",
    products: [
        .library(
            name: "BGGKit",
            targets: ["BGGKit"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/danielctull-public/resourceful", .branch("master")),
        .package(url: "https://github.com/MaxDesiatov/XMLCoder", .branch("master"))
    ],
    targets: [
        .target(
            name: "BGGKit",
            dependencies: [
                "Resourceful",
                "XMLCoder",
            ]),
        .testTarget(
            name: "BGGKitTests",
            dependencies: ["BGGKit"]),
    ]
)
